class CreateBooks < ActiveRecord::Migration[5.2]
  def change
    create_table :books do |t|
      t.string :title
      t.string :author
      t.string :publisher
      t.string :publish_date
      t.integer :page_count
      t.float :book_price
      t.string :isbn_10
      t.string :isbn_13
      t.text :description

      t.timestamps
    end
  end
end
