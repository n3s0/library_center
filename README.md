# Library Center

Library Center was created out of my wanting to learn Ruby on Rails and my need to have an inventory/library application for my books. "Why recreate the wheel?" some may ask, there are library applications already available. Well, I wanted to learn Ruby on Rails and I wanted this application to suite my needs. This README will discuss the features of Library Center and methods of which you can install it. There will be no features presented at this time because I am not finished. But I will show how to install the RoR application in the Library Center section.

A lot of the code in this project can provide thanks to a YouTuber named Ryan Hemrick. As a majority of this project's construction is based on his tutorials I will be providing links to his YouTube channel. Note though, there were quite a few things that needed to be done myself. Sadly, Paperclip is no longer with us as a supported library in Ruby on Rails. I didn't just fork the repository because at the time I didn't understand a thing about Rails, I had not hands-on experience. I needed that. But, regardless, since his tutorials have been a great help to me on the development of this project, I will be displaying links to the YouTube channel and videos, along with his GitHub page.

I would also like to note that this is just a general setup guide. For more details information on how to setup, I will be creating a Wiki for use. Please consult that when that has been created.

* Wiki: No Wiki has been created for this project.

#### Ryan Hemricks Links (As a thank you for the help):

  * [Ryan Hemrick's YouTube Channel](https://www.youtube.com/user/IHemrick)
  * [Ryan Hemrick's Ruby on Rails - Book Review Application Tutorial Series](https://www.youtube.com/playlist?list=PLsyJtAEU4pGw3PCKAA4POHK6C2G2nnpLF)
  * [Ryan Hemrick's GitHub](https://github.com/RyanHemrick?tab=repositories)

### Where I can find this application?

  * [GitLab - Library Center](https://gitlab.com/baffoon/LibraryCenter.git)

## Library Center Installation

### Current Supported/Tested Platforms (Development)

  * Ubuntu 18.04 LTS Bionic Beaver

### Current Supported/Tested Platforms (Production)

  * None

### Current Supported/Tested Database Platforms (Development)

  * SQLite3
  * PostgreSQL 9.5

### Current Supported/Tested Database Platforms (Production)

  * None

### Ruby & Rails Versions

  * Ruby Version: ruby 2.5.3p105
  * Ruby on Rails Version: Rails 5.2.1

### Dependencies

  * Ruby & Ruby on Rails Version:
    * Ruby 2.5.3p105
    * Rails 5.2.1
  * SQLite3 (Not Recommended)
  * PostgreSQL 9.5 (Recommended)
  * MySQL (Optional)

### Installation Instructions

Here I will be showing you how to install the application. You will want to start by cloning the application with git. These are manual instructions for setting up the application for an RVM environment. Soon, I hope, there will be an installation script created that will install the application. Regardless I will keep the installation instructions available for documentation purposes. These instructions should also be available in a wiki, should I ever make one.

Note: If you need assistance on installing ruby on rails I will provide a way shortly.

#### PostgreSQL Installation (Recommended)

For instructions on how to install PostgreSQL onto your machine, please consult the Wiki. Wiki link should be at the top of the page.

I am not a fan of allowing the user of an application the ability to create databases. But, if that is your cup of tea, below is how you would do it. I would not recommend this however. I would also note that you should also consider changing the password in these examples and then changing them in the config/database.yml file.

```
CREATE ROLE librarycenterdb_admin WITH CREATEDB LOGIN PASSWORD 'S3curity';
```

Now for my preferred method. This may create more work for you, but it will also protect your PostgreSQL database in the event that the database user created is compromised.

First you will want to login to your PostgreSQL console using the following command:

```
psql -h localhost -U postgres
```

If you are not using a local database, make that you have configured your database to accept connections from the rails application and replace localhost with the name of the database server.

Then you will want to create the user librarycenterdb_admin with a password.

```
CREATE ROLE librarycenterdb_admin WITH PASSWORD 'S3curity';
```

Once this is finished, you will want to create the following databases:

* librarycenterdb
* librarycenterdb_test
* librarycenterdb_dev

This is so you have all three databases in the configuration at your disposal. You may omit them if you plan on putting this in production. I would recommend checking the status of the production build before doing so. You can use the following commands to create the databases.

```
CREATE DATABASE librarycenterdb
CREATE DATABASE librarycenterdb_test
CREATE DATABASE librarycenterdb_dev
```


#### MySQL Installation (NOT TESTED)

This will be created shortly. It has not been tested yet.

#### GitLab

```
$ git clone https://www.gitlab.com/baffoon/LibraryCenter.git
```

Then you will want to go into the ```librarycenter``` or ```LibraryCenter``` directory.

```
$ cd LibraryCenter
```

I am using this in an RVM environment. So if you don't have RVM installed, you will need to install it as per the dependencies above. I have not yet setup a production build because this is still in development.

You will need to install the necessary gems using the following command:

```
$ bundle install
```

Then you will want to undergo the initial installation for the simple_form gem for the forms:

```
$ rails generate simple_form:install --bootstrap
```

Below is for installing devise in the application. Devise is used for user generation and it will handle the login/out functionality for the application.

To do this really all you need to do is migrate the database:

```
$ rails db:migrate
$ rails db:seed
```

Please note that categories are not created for books by default and there is not functionality for doing so just yet. I will be adding it though. For now you will have to use the rails console to do this.


#### TODO for this README

Once the TODO is finished, you will not see it anymore.

  * System dependencies (Almost finished)
  * Configuration
  * Database creation
  * Database initialization
  * How to run the test suite
  * Services (job queues, cache servers, search engines, etc.)
  * Deployment instructions
  * Write a script for deployment
