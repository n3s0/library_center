class BooksController < ApplicationController
  before_action :find_book, only: [:show, :edit, :update, :destroy]

  # General index for the application. This is where you see the overview of books.
  def index
    if params[:category].blank?
      @books = Book.all.order("created_at DESC")
    else
      @category_id = Category.find_by(name: params[:category]).id
      @books = Book.where(:category_id => @cateogory_id).order("created_at DESC")
    end
  end

  # For showing a book or books.
  def show
  end

  def new
    #@book = Book.new
    @book = current_user.books.build
    # This will map all of the categories for me and put them into a drop down.
    @categories = Category.all.map{ |c| [c.name, c.id] }
  end

  def create
    #@book = Book.new(book_params)
    @book = current_user.books.build(book_params)
    @book.category_id = params[:category_id]
    @book.book_image.attach(params[:book][:book_image])

    # If save is intiated, redirect to the root path to see the new book.
    if @book.save
      redirect_to root_path
    else
      # Else you will re-render the new page.
      render 'new'
    end
  end

  # For editing books
  def edit
    # Load all of the categories so you can changed them for he book.
    @categories = Category.all.map{ |c| [c.name, c.id] }
  end

  # For updating a book.
  def update
    @book.category_id = params[:category_id]
    @book.book_image.purge
    @book.book_image.attach(params[:book_image])
    if @book.update(book_params)
      # Will redirect to the book's show page so you can view it.
      redirect_to book_path(@book)
    else
      # Else you will re-render the edit page.
      render 'edit'
    end
  end

  def destroy
    @book.destroy
    redirect_to root_path
  end

  # Private method for hidden parameters.
  private
    # These are the book_params for the appliation. These are required if the user is to view, edit, or destroy a book.
    def book_params
      params.require(:book).permit(:title, :author, :publisher, :publish_date, :book_price, :page_count, :isbn_10, :isbn_13, :description, :category_id, :book_image)
    end

    # The find_book method for the application. This will find all of the books for you.
    def find_book
      @book = Book.find(params[:id])
    end

end
