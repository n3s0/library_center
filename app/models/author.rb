class Author < ApplicationRecord
	# Authors can produce multiple books.
	has_many :books

	# Authors can belong to many publishers throughout their life time.
	has_many :publishers 
end
