class Review < ApplicationRecord
  # Reviews can belong to a book.
  belongs_to :book
  # Reviews can belong to a user.
  belongs_to :user
end
