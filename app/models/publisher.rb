class Publisher < ApplicationRecord
	# Can have many authors working for them.
	has_many :authors
	
	# Publishers can publish many books.
	has_many :books
end
