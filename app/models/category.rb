class Category < ApplicationRecord
  # Association for the Category model
  has_many :books # One category has many books
end
