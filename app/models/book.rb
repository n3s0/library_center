# Associations for the Book model
class Book < ApplicationRecord
  # Book can belong to one user.
  belongs_to :user 
  
  # Multiple authors can publish a book.
  has_many :authors
  
  # Book can only belong to one category.
  belongs_to :category
  
  # A book can have many reviews.
  has_many :reviews
  
  # Can only have one attached image of the book.
  has_one_attached :book_image
end
